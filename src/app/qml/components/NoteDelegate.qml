/*
 * Copyright: 2020 UBports
 *
 * This file is part of reminders
 *
 * reminders is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * reminders is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.4
import Ubuntu.Components 1.3
import Evernote 0.1

ListItem {
    id: root
    height: units.gu(12)

    property var note
    property string notebookColor: preferences.colorForNotebook(note.notebookGuid)
    property string tags: {
        var tags = new Array()
        for(var i = 0; i < note.tagGuids.length; i++) {
            tags.push(NotesStore.tag(note.tagGuids[i]).name)
        }
        return tags.join(" ")
    }

    signal openNote()
    signal deleteNote()
    signal editNote()
    signal editReminder()
    signal editTags()

    leadingActions: ListItemActions {
        actions: [
            Action {
                iconName: "delete"
                enabled: !root.note.conflicting

                onTriggered: root.deleteNote()
            }
        ]
    }
    action: Action {
        onTriggered: root.openNote()
    }
    trailingActions: ListItemActions {
        actions: [
            Action {
                iconName: "alarm-clock"
                enabled: !root.note.conflicting

                onTriggered: root.editReminder()
            },
            Action {
                iconName: "tag"
                enabled: !root.note.conflicting

                onTriggered: root.editTags()
            },
            Action {
                iconName: "edit"
                enabled: !root.note.conflicting

                onTriggered: root.editNote()
            }
        ]
    }

    Column {
        anchors.fill: parent

        Rectangle {
            id: notebookLine
            width: parent.width
            height: units.gu(0.5)
            color: root.notebookColor
        }

        Item {
            width: parent.width
            height: parent.height - notebookLine.height

            Row {
                anchors.fill: parent
                anchors.margins: units.gu(1)
                spacing: units.gu(1)

                Column {
                    width: parent.width - parent.spacing - stateIcons.width
                    height: parent.height

                    Label {
                        id: titleLabel
                        width: parent.width
                        text: root.note.title
                        textSize: Label.Medium
                        color: root.notebookColor
                        font.strikeout: root.note.deleted
                    }

                    Label {
                        id: dateLabel
                        width: parent.width
                        text: Qt.formatDateTime(root.note.updated, Qt.LocalDate)
                        textSize: Label.Small
                        color: root.notebookColor
                    }

                    Label {
                        width: parent.width
                        height: parent.height - titleLabel.height - dateLabel.height - tagsLabel.height
                        text: root.note.tagline
                        textSize: Label.Small
                        maximumLineCount: 2
                        wrapMode: Text.WrapAtWordBoundaryOrAnywhere
                        font.strikeout: root.note.deleted
                        textFormat: Text.PlainText
                    }

                    Label {
                        id: tagsLabel
                        width: parent.width
                        text: root.tags
                        textSize: Label.XSmall
                        maximumLineCount: 1
                        wrapMode: Text.WordWrap
                    }
                }

                Item {
                    id: stateIcons
                    width: units.gu(2)
                    height: parent.height

                    Icon {
                        anchors {
                            top: parent.top
                            right: parent.right
                            left: parent.left
                        }
                        height: width
                        name: "alarm-clock"
                        visible: root.note.reminder
                    }
                    Icon {
                        anchors {
                            right: parent.right
                            bottom: parent.bottom
                            left: parent.left
                        }
                        height: width
                        name: root.note.loading ? "sync-updating" :
                              root.note.syncError ? "sync-error" :
                              root.note.synced ? "sync-idle" :
                              root.note.conflicting ? "weather-severe-alert-symbolic" : "sync-offline"
                    }
                }
            }
        }

    }
}
