/*
 * Copyright: 2013 Canonical, Ltd
 *
 * This file is part of reminders
 *
 * reminders is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * reminders is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.4
import Ubuntu.Components 1.3
import Ubuntu.Components.Popups 1.3
import Evernote 0.1
import Ubuntu.Components.Themes.Ambiance 1.3

Page {
    id: notePage

    property alias note: noteView.note

    signal editNote(var note)

    header: PageHeader {
        id: notePageHeader

        property string notebookColor: preferences.colorForNotebook(notePage.note.notebookGuid)

        title: note.title
        style: PageHeaderStyle {
            foregroundColor: notePageHeader.notebookColor
        }

        extension: Rectangle {
            id: notePageHeaderExtension

            width: notePageHeader.width
            height: units.gu(1)
            color: notePageHeader.notebookColor
        }

        trailingActionBar {
            actions: [
                Action {
                    iconName: "edit"

                    onTriggered: {
                        notePage.editNote(note)
                    }
                },
                Action {
                    iconName: "tag"

                    onTriggered: {
                        PopupUtils.open(Qt.resolvedUrl("../components/EditTagsDialog.qml"), notePage, { note: notePage.note })
                    }
                },
                Action {
                    iconName: "reminder"

                    onTriggered: {
                        pagestack.push(Qt.resolvedUrl("SetReminderPage.qml"), { note: notePage.note })
                    }
                }
            ]
        }
    }

    NoteView {
        id: noteView

        anchors {
            top: notePageHeader.bottom
            right: notePage.right
            bottom: notePage.bottom
            left: notePage.left
        }

        onEditNote: {
            notePage.editNote(notePage.note)
        }
    }
}
